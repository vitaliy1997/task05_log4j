package com.log.word;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WordPoem {

    private static Logger logger = LogManager.getLogger(WordPoem.class);

    public void Word() {
        logger.trace("When I am dead");
        logger.debug("bury me In my beloved Ukraine");
        logger.info("My tomb upon a grave mound high Amid the spreading plain");
        logger.warn("So that the fields");
        logger.error("the boundless steppes");
        logger.fatal("The Dnieper’s plunging shore My eyes could see");
        logger.fatal("my ears could hear The mighty river roar.");
    }
}
