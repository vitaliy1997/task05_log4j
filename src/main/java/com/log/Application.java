package com.log;

import com.log.book.Book;
import com.log.word.WordPoem;
import com.log.writer.Writer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        Book book = new Book("Shevchenko", "Zapovit");
            book.generateMessage();
            book.Exception();

        Writer writer = new Writer("Franko","Kateryna",1844,"Lviv district");
        writer.notOverwrite();

        WordPoem wordPoem = new WordPoem();
        wordPoem.Word();

        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");

    }
}
